// Copyright 2022 Tobias Berdin


// override navigator.credentials.create
navigator.credentials.create = function (pk) {
    return new Promise(async (resolve, reject) => {
        // update ui
        document.getElementById("authenticator-overlay-header").innerHTML = "EPID Auth &#9203;"

        // perform session request
        let create = new XMLHttpRequest()
        create.open("POST", document.getElementById('authenticator-remote-instance').innerText + "/session", false)
        create.send()
        if (create.status !== 200) {
            // finish upon error
            document.getElementById("authenticator-overlay-header").innerHTML = "EPID Auth &#128293;"
            reject("Error while creating session")
        }
        let response = JSON.parse(create.responseText)
        let challenge = response.challenge
        let session = response.session

        // assemble sign request
        let signRequest = {}
        signRequest.privateKey = toByteArray(document.getElementById('authenticator-member-key').innerText)
        signRequest.publicKey = toByteArray(document.getElementById('authenticator-group-key').innerText)
        signRequest.message = challenge

        // perform sign request
        let sign = new XMLHttpRequest()
        sign.open("POST", document.getElementById('authenticator-epid-instance').innerText + "/signmsg", false)
        sign.send(JSON.stringify(signRequest))
        if (sign.status !== 200) {
            // finish upon error
            document.getElementById("authenticator-overlay-header").innerHTML = "EPID Auth &#128293;"
            let err = {}
            err.message = "Error while signing"
            reject(err)
        }
        response = JSON.parse(sign.responseText)
        let signature = response.signature

        // assemble create request
        pk.publicKey.challenge = normalizeArray(pk.publicKey.challenge)
        pk.publicKey.user.id = normalizeArray(pk.publicKey.user.id)
        pk.EPIDPublicKey = toByteArray(document.getElementById('authenticator-group-key').innerText)
        pk.session = session
        pk.signature = signature

        // perform create request
        let verify = new XMLHttpRequest()
        verify.open("POST", document.getElementById('authenticator-remote-instance').innerText + "/create", false)
        verify.send(JSON.stringify(pk))
        if (verify.status !== 200) {
            // finish upon error
            document.getElementById("authenticator-overlay-header").innerHTML = "EPID Auth &#128293;"
            reject("Error while verifying")
        }

        // update ui
        document.getElementById('authenticator-overlay-header').innerHTML = "EPID Auth &#9989;"

        resolve(JSON.parse(verify.responseText))
    })
}

// override nagivator.credentials.get
navigator.credentials.get = function (pk) {
    return new Promise(async (resolve, reject) => {
        // update ui
        document.getElementById("authenticator-overlay-header").innerHTML = "EPID Auth &#9203;"

        // perform session request
        let create = new XMLHttpRequest()
        create.open("POST", document.getElementById('authenticator-remote-instance').innerText + "/session", false)
        create.send()
        if (create.status !== 200) {
            // finish upon error
            document.getElementById("authenticator-overlay-header").innerHTML = "EPID Auth &#128293;"
            let err = {}
            err.message = "Error while creating session"
            reject(err)
        }
        let response = JSON.parse(create.responseText)
        let challenge = response.challenge
        let session = response.session

        // assemble sign request
        let signRequest = {}
        signRequest.privateKey = toByteArray(document.getElementById('authenticator-member-key').innerText)
        signRequest.publicKey = toByteArray(document.getElementById('authenticator-group-key').innerText)
        signRequest.message = challenge

        // perform sign request
        let sign = new XMLHttpRequest()
        sign.open("POST", document.getElementById('authenticator-epid-instance').innerText + "/signmsg", false)
        sign.send(JSON.stringify(signRequest))
        if (sign.status !== 200) {
            // finish upon error
            document.getElementById("authenticator-overlay-header").innerHTML = "EPID Auth &#128293;"
            let err = {}
            err.message = "Error during EPID signing"
            reject(err)
        }
        response = JSON.parse(sign.responseText)
        let signature = response.signature

        // assemble create request
        for (let i = 0; i < pk.publicKey.allowCredentials.length; i++) {
            pk.publicKey.allowCredentials[i].id = normalizeArray(pk.publicKey.allowCredentials[i].id)
        }
        pk.publicKey.challenge = normalizeArray(pk.publicKey.challenge)
        pk.EPIDPublicKey = toByteArray(document.getElementById('authenticator-group-key').innerText)
        pk.session = session
        pk.signature = signature

        // perform create request
        let verify = new XMLHttpRequest()
        verify.open("POST", document.getElementById('authenticator-remote-instance').innerText + "/authenticate", false)
        verify.send(JSON.stringify(pk))
        if (verify.status !== 200) {
            // finish upon error
            document.getElementById("authenticator-overlay-header").innerHTML = "EPID Auth &#128293;"
            let err = {}
            err.message = "Error during EPID verification"
            reject(err)
        }

        // update ui
        document.getElementById('authenticator-overlay-header').innerHTML = "EPID Auth &#9989;"

        resolve(JSON.parse(verify.responseText))
    })
}


/**
 * Normalizes a given array so that it can be easily converted to a json string.
 */
function normalizeArray(uint8array) {
    let c = []
    for (let i = 0; i < uint8array.length; i++) {
        c.push(uint8array[i])
    }
    return c
}


/**
 * Returns an int array from a hex string of the form "\x41\x41\x41...".
 */
function toByteArray(str) {
    let a = []
    let parts = str.split("\\x")
    for (let c = 1; c < parts.length; c++) {
        a.push(parseInt(parts[c], 16))
    }
    return a
}