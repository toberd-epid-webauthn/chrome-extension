// Saves options to chrome.storage
function save_options() {
    const memberKey = document.getElementById('memberKey').value
    const groupKey = document.getElementById('groupKey').value
    const epidInstance = document.getElementById('epidInstance').value
    const remoteInstance = document.getElementById('remoteInstance').value
    chrome.storage.local.set({
        memberKey: memberKey,
        groupKey: groupKey,
        epidInstance: epidInstance,
        remoteInstance: remoteInstance
    }, function () {
        // Update status to let user know options were saved.
        const status = document.getElementById('status')
        status.textContent = "Updated"
    });
}

function onload() {
    const currentMemberKey = document.getElementById('memberKey')
    chrome.storage.local.get(['memberKey'], function(key) {
        if (key != null) {
            currentMemberKey.value = key.memberKey
        }
    })
    const currentGroupKey = document.getElementById('groupKey')
    chrome.storage.local.get(['groupKey'], function(key) {
        if (key != null) {
            currentGroupKey.value = key.groupKey
        }
    })
    const currentEpidInstance = document.getElementById('epidInstance')
    chrome.storage.local.get(['epidInstance'], function(instance) {
        if (instance != null) {
            currentEpidInstance.value = instance.epidInstance
        }
    })
    const currentRemoteInstance = document.getElementById('remoteInstance')
    chrome.storage.local.get(['remoteInstance'], function(instance) {
        if (instance != null) {
            currentRemoteInstance.value = instance.remoteInstance
        }
    })
}

onload()
document.getElementById('save').addEventListener('click',
    save_options)