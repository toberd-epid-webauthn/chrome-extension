// Copyright 2022 Tobias Berdin

loadStorage()

// inject content of script.js to webpage
const s = document.createElement('script')
s.src = chrome.runtime.getURL('script.js')
s.onload = function () {
    this.remove();
};
(document.head || document.documentElement).appendChild(s);


function loadStorage() {
    // prepare overlay
    const overlay = document.createElement('div')
    overlay.id = "authenticator-overlay"
    const overlayHeader = document.createElement('p')
    overlayHeader.id = "authenticator-overlay-header"
    overlayHeader.innerHTML = "EPID Auth &#9989;"
    overlay.appendChild(overlayHeader)

    // add keys to document page
    const m = document.createElement('span')
    m.style.display = "none"
    m.id = "authenticator-member-key"
    chrome.storage.local.get(['memberKey'], function(key) {
        if (key.memberKey) {
            m.innerText = key.memberKey
            document.body.appendChild(m)
        } else {
            overlayHeader.innerHTML = "EPID Auth &#10060;"
        }
    })
    const g = document.createElement('span')
    g.style.display = "none"
    g.id = "authenticator-group-key"
    chrome.storage.local.get(['groupKey'], function(key) {
        if (key.groupKey) {
            g.innerText = key.groupKey
            document.body.appendChild(g)
        } else {
            overlayHeader.innerHTML = "EPID Auth &#10060;"
        }
    })

    // add instance to document page
    const i = document.createElement('span')
    i.style.display = "none"
    i.id = "authenticator-epid-instance"
    chrome.storage.local.get(['epidInstance'], function(instance) {
        i.innerText = instance.epidInstance
        document.body.appendChild(i)
    })

    // add remote to document page
    const r = document.createElement('span')
    r.style.display = "none"
    r.id = "authenticator-remote-instance"
    chrome.storage.local.get(['remoteInstance'], function(instance) {
        r.innerText = instance.remoteInstance
        document.body.appendChild(r)
    })

    document.body.appendChild(overlay)
}